'use client'

import MessageStyle from '@/app/lib/main/message/AllMessage.module.css';
import data from '@/app/stores/data';
import { useEffect, useState } from 'react';

export default function Message() {
    const [listMess, setListMess] = useState([{
        id: 1,
        body: '',
        user: {
            username: ''
        }
    }])

    const [mess, setMess] = useState({
        id: 0,
        body: '',
        user: {
            username: 'No name'
        }
    })

    const [chat_content, setChatContent] = useState([{
        id: 1,
        title: ''
    }])

    const [chat_selected, setChatSelected] = useState(0);

    useEffect(() => {
        const api = 'https://dummyjson.com/comments?limit=30';
        fetch(api) 
        .then(res => res.json())
        .then(data => {
            setListMess(data.comments);
        })
    },[])

    function see_detailed_message(s: string) {
        setChatSelected(parseInt(s));
        const api = 'https://dummyjson.com/comments/';
        fetch(api + s)
        .then(res => res.json())
        .then(data => {
            setMess(data);
        })
        const api_chat = 'https://dummyjson.com/posts?select=id,title';
        fetch(api_chat)
        .then(res => res.json())
        .then(data => {
            setChatContent(data.posts)
        })
    }

    

    const now = new Date();
    let date = now.getDate().toString();
    let month = (parseInt(now.getMonth().toString()) + 1).toString();
    if(month.length == 1) {
        month = '0' + month;
    }
    if(date.length == 1) {
        date = '0' + date;
    }
    const year = now.getFullYear().toString();

    return(
        <div className={MessageStyle.container}>
            <div className={MessageStyle.title}>
                <p>My message</p>
                <a href="/">Back</a>
            </div>
            <div className={MessageStyle.content}>
                <div className={MessageStyle.listMess}>
                    <div className={MessageStyle.search_name}>
                        <input type="text" />
                        <button>Search</button>
                    </div>
                    <div className={MessageStyle.all_chat}>
                        {listMess.map((mess) => {
                            return(
                                <div key={mess.id} onClick={() => see_detailed_message(mess.id.toString())}
                                    className={chat_selected == mess.id ? MessageStyle.chat_selected : MessageStyle.chat_default}
                                >
                                    <p>{mess.user.username}</p>
                                    <span>{mess.body}</span>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <div className={MessageStyle.detail}>
                    <div className={MessageStyle.username}>
                        <span>{mess.user.username}</span>
                    </div>
                    <div className={MessageStyle.body}>
                        {chat_content.map(chat => {
                            if(mess.id == 0) {
                                return(
                                    <div>
                                        <span>Select a chat from the list</span>
                                    </div>
                                )
                            } else if(mess.id == 1){
                                if(chat.id % 2 == 0) {
                                    return(
                                        <div key={chat.id} className={MessageStyle.received}>
                                            <span>{chat.title}</span>
                                        </div>
                                    )
                                } else {
                                    return(
                                        <div key={chat.id} className={MessageStyle.sent}>
                                            <span>{chat.title}</span>
                                        </div>
                                    )
                                }
                            } else {
                                if(chat.id % mess.id == 0) {
                                    return(
                                        <div key={chat.id} className={MessageStyle.received}>
                                            <span>{chat.title}</span>
                                        </div>
                                    )
                                } else {
                                    return(
                                        <div key={chat.id} className={MessageStyle.sent}>
                                            <span>{chat.title}</span>
                                        </div>
                                    )
                                }
                            }
                            
                        })}
                    </div>
                    <div className={MessageStyle.send}>
                        <input type="text" />
                        <button>Send</button>
                    </div>
                </div>
            </div>
            <div className={MessageStyle.footer}>
                <p>{date} - {month} - {year}</p>
            </div>
        </div>
    )
}