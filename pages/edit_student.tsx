import EditStyle from "@/app/lib/main/EditStyle.module.css";
import Link from "next/link";
import { useEffect, useState } from "react";
export default function AddNewStudent() {   
    const [avt, setAvt] = useState('https://images.rawpixel.com/image_800/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTA1L3Y5MzctYWV3LTEzOS5qcGc.jpg');
    const avt1 = 'https://i.pinimg.com/564x/2b/0f/7a/2b0f7a9533237b7e9b49f62ba73b95dc.jpg';
    const avt2 = 'https://chiemtaimobile.vn/images/companies/1/%E1%BA%A2nh%20Blog/avatar-facebook-dep/Anh-avatar-hoat-hinh-de-thuong-xinh-xan.jpg?1704788263223';
    const avt3 = 'https://cdn.sforum.vn/sforum/wp-content/uploads/2023/11/avatar-dep-107.jpg';
    const avt4 = 'https://hoanghamobile.com/tin-tuc/wp-content/uploads/2023/07/avatar-dep-4.jpg';
    const avt5 = 'https://gcs.tripi.vn/public-tripi/tripi-feed/img/474103Wsh/hinh-avatar-dep-1.jpg';
    const avt6 = 'https://kiemtientuweb.com/ckfinder/userfiles/images/avatar-fb/avatar-fb-1.jpg';
    const [isListAvt, setIsListAvt] = useState(false);
    let student1 = {
        firstName: '',
        lastName: '',
        age: '',
        birthDate: '',
        gender: 'male',
        email: '',
        image: avt,
        phone: '',
        address: {
            address: '',
            city: ''
        },
        weight: '',
        height: '',
        bloodGroup: '',
        hair: {
            color: '',
            type: '' 
        }
    }

    const [age, setAge] = useState('');
    const [student2, setStudent2] = useState(
        {
            id: 0,
            firstName: '',
            lastName: '',
            age: 0,
            birthDate: '',
            gender: '',
            email: '',
            image: '',
            phone: '',
            address: {
                address: '',
                city: ''
            },
            weight: '',
            height: '',
            bloodGroup: '',
            hair: {
                color: '',
                type: ''
            }
        }
    );
    
    useEffect(() => {
        const id_student = localStorage.getItem('id_student');
        const api = 'https://dummyjson.com/users/';
        const select = '?select=id,firstName,lastName,age,birthDate,gender,email,image,phone,address,weight,height,bloodGroup,hair';
        fetch(api+id_student+select)
        .then((res) => res.json())
        .then((data) => {
            setStudent2(data);
            setAvt(data.image);
            setAge(data.age);
            student1.firstName = data.firstName;
            student1.lastName = data.lastName;
            student1.email = data.email;
            student1.age = data.age;
            student1.birthDate = data.birthDate;
            student1.gender = data.gender;
            student1.phone = data.phone;
            student1.weight = data.weight;
            student1.height = data.height;
            student1.address.address = data.address.adress;
            student1.address.city = data.address.city;
            student1.bloodGroup = data.bloodGroup;
        });
    }, [])
    
    const now = new Date();
    let date = now.getDate().toString();
    let month = (parseInt(now.getMonth().toString()) + 1).toString();
    if(month.length == 1) {
        month = '0' + month;
    }
    if(date.length == 1) {
        date = '0' + date;
    }
    const year = now.getFullYear().toString();
    const day_now = now.getFullYear().toString().trim() + "-12-31";
    function showAvt(s: string) {
        setAvt(s);
        setIsListAvt(false);
        student1.image = s;
    }
    function showSelectAvt() {
        setIsListAvt(!isListAvt);
    }
    function setFirstName(s: string) {
        student1.firstName = s.trim();
    }
    function setLastName(s: string) {
        student1.lastName = s.trim();
    }
    function setEmail(s: string) {
        student1.email = s.trim();
    }
    function setGender(s: string) {
        student1.gender = s;
    }
    function setBirthDate(s: string) {
        student1.birthDate = s;
        student1.age = (now.getFullYear() - parseInt(s.slice(0, 4))).toString();
        setAge(student1.age);
    }
    function setPhone(s: string) {
        student1.phone = s.trim();
    }
    function setAddressAdress(s: string) {
        student1.address.address = s.trim();
    }
    function setAddressCity(s: string) {
        student1.address.city = s.trim();
    }
    function setWeight(s: string) {
        student1.weight = s;
    }
    function setHeight(s: string) {
        student1.height = s;
    }
    function setBloodGroup(s: string) {
        student1.bloodGroup = s;
    }
    function setHairColor(s: string) {
        student1.hair.color = s.trim();
    }
    function setHairType(s: string) {
        student1.hair.type = s.trim();
    }

    function checkForEmpty(s: string, t: string) {
        if(s == '') {
            s = t;
        }
    }

    function addNewStudent() {
        try{
            const api = 'https://dummyjson.com/users/'
            fetch(api + student2.id, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                firstName: student1.firstName,
                lastName: student1.lastName,
                age: student1.age,
                birthDate: student1.birthDate,
                gender: student1.gender,
                email: student1.email,
                image: student1.image,
                phone: student1.phone,
                address: {
                    address: student1.address.address,
                    city: student1.address.city
                },
                weight: student1.weight,
                height: student1.height,
                bloodGroup: student1.bloodGroup,
                hair: {
                    color: student1.hair.color,
                    type: student1.hair.type
                }
                })
            })
            .then(res => res.json())
            .then(console.log)
            alert("Edit student's information successfully!!");
        } catch(e) {
            alert("Edit student's information failed!!");
            console.log(e);
        } finally {
            student1 = {
                firstName: '',
                lastName: '',
                age: '',
                birthDate: '',
                gender: 'male',
                email: '',
                image: avt,
                phone: '',
                address: {
                    address: '',
                    city: ''
                },
                weight: '',
                height: '',
                bloodGroup: '',
                hair: {
                    color: '',
                    type: '' 
                }
            }
        }
    }

    return(
        <div className={EditStyle.container}>
            <div className={EditStyle.header}>
                <Link href={{pathname: 'student', query: {id: student2.id}}}>
                    Back
                </Link>
                <a href="/">
                    Student Mangagement
                </a>
            </div>
            <div className={EditStyle.content}>
                <div>
                    <div className={EditStyle.avt}>
                        <img src={avt} alt="" />
                    </div>
                    <div className={EditStyle.select_avt}>
                        <button onClick={() => showSelectAvt()}>Select an image</button>
                        <div className={isListAvt ? EditStyle.list_avt : EditStyle.list_avt_hide}>
                            <div className={EditStyle.img1} onClick={() => showAvt(avt1)}>Img 1</div>
                            <div className={EditStyle.img2} onClick={() => showAvt(avt2)}>Img 2</div>
                            <div className={EditStyle.img3} onClick={() => showAvt(avt3)}>Img 3</div>
                            <div className={EditStyle.img4} onClick={() => showAvt(avt4)}>Img 4</div>
                            <div className={EditStyle.img5} onClick={() => showAvt(avt5)}>Img 5</div>
                            <div className={EditStyle.img6} onClick={() => showAvt(avt6)}>Img 6</div>
                        </div>
                    </div>
                </div>
                <div className={EditStyle.infor}>
                    <div className={EditStyle.about}>
                        <p>About</p>
                    </div>
                    <table className={EditStyle.tbl}>
                        <colgroup>
                            <col width={100}/>
                            <col width={280}/>
                            <col width={100}/>
                            <col width='*'/>
                        </colgroup>
                        <thead></thead>
                        <tbody>
                            <tr className={EditStyle.tr1}>
                                <td>First name:</td>
                                <td>
                                    <input type="text" onChange={(e) => setFirstName(e.target.value)} defaultValue={student2.firstName}/>
                                </td>
                                <td>Last name:</td>
                                <td>
                                    <input type="text" onChange={(e) => setLastName(e.target.value)} defaultValue={student2.lastName}/>
                                </td>
                            </tr>
                            <tr className={EditStyle.tr2}>
                                <td>Email:</td>
                                <td colSpan={3}>
                                    <input type="email" onChange={(e) => setEmail(e.target.value)} defaultValue={student2.email}/>
                                </td>
                            </tr>
                            <tr className={EditStyle.tr4}>
                                <td>Birthdate:</td>
                                <td colSpan={3}>
                                    <input type="date" name="" id="" 
                                        onChange={(e) => setBirthDate(e.target.value)}
                                        min="1990-01-01"
                                        max={day_now}
                                        defaultValue={student2.birthDate}
                                    />
                                </td>
                            </tr>
                            <tr className={EditStyle.tr3}>
                                <td>Age:</td>
                                <td>
                                    <input type="text" placeholder={age} disabled/>
                                </td>
                                <td>Gender:</td>
                                <td>
                                    <input type="radio" name="gender" id="" value="male" defaultChecked={student2.gender == "male" ? true : false} onChange={(e) => setGender(e.target.value)} />
                                    <label htmlFor="">Male</label>
                                    <input type="radio" name="gender" id="" value="female" defaultChecked={student2.gender == "female" ? true : false} onChange={(e) => setGender(e.target.value)}/>
                                    <label htmlFor="">Female</label>
                                </td>
                            </tr>
                            <tr className={EditStyle.tr5}>
                                <td>Phone:</td>
                                <td colSpan={3}>
                                    <input type="text" name="" id="" defaultValue={student2.phone} onChange={(e) => setPhone(e.target.value)}/>
                                </td>
                            </tr>
                            <tr className={EditStyle.tr6}>
                                <td>Address:</td>
                                <td>
                                    <input type="text" name="" id="" defaultValue={student2.address.address} onChange={(e) => setAddressAdress(e.target.value)}/>
                                </td>
                                <td>City:</td>
                                <td>
                                    <input type="text" defaultValue={student2.address.city} onChange={(e) => setAddressCity(e.target.value)}/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className={EditStyle.health}>
                        <p>Health</p>
                    </div>
                    <table className={EditStyle.tbl}>
                        <colgroup>
                            <col width={100}/>
                            <col width={280}/>
                            <col width={100}/>
                            <col width='*'/>
                        </colgroup>
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Weight (kg): </td>
                                <td>
                                    <input type="number" name="" id="" max={1000} min={1} 
                                        onChange={(e) => setWeight(e.target.value)}
                                        defaultValue={student2.weight}
                                    />
                                </td>
                                <td>Height (cm): </td>
                                <td>
                                    <input type="number" min={1} onChange={(e) => setHeight(e.target.value)}
                                        defaultValue={student2.height}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Blood Group: </td>
                                <td colSpan={3}>
                                    <select name="" id="" onChange={(e) => setBloodGroup(e.target.value)}
                                        defaultValue={student2.bloodGroup}
                                    >
                                        <option value="select">Select</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Hair color:</td>
                                <td>
                                    <input type="text" onChange={(e) => setHairColor(e.target.value)} defaultValue={student2.hair.color}/>
                                </td>
                                <td>Hair type:</td>
                                <td>
                                    <input type="text" onChange={(e) => setHairType(e.target.value)} defaultValue={student2.hair.type}/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className={EditStyle.btn}>
                        <button onClick={() => addNewStudent()}>Save</button>
                        <button>Cancel</button>
                    </div>
                </div>
            </div>
            <div className={EditStyle.footer}>
                <p>{date} - {month} - {year}</p>
            </div>
        </div>
        
    )
}