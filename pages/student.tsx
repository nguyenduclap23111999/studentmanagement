'use client'

import Image from "next/image";
import { useEffect, useState } from "react";
import StudentStyle from "@/app/lib/main/Student.module.css";
import { title } from "process";
import Link from "next/link";

export default function Student() {
    const [student, setStudent] = useState(
        {
            id: 0,
            firstName: '',
            lastName: '',
            age: 0,
            birthDate: '',
            gender: '',
            email: '',
            image: '',
            phone: '',
            address: {
                address: '',
                city: ''
            },
            weight: '',
            height: '',
            bloodGroup: '',
            hair: {
                color: '',
                type: ''
            }
        }
    );
    
    useEffect(() => {
        const id_student = localStorage.getItem('id_student');
        const api = 'https://dummyjson.com/users/';
        const select = '?select=id,firstName,lastName,age,birthDate,gender,email,image,phone,address,weight,height,bloodGroup,hair';
        fetch(api+id_student+select)
        .then((res) => res.json())
        .then((data) => setStudent(data));
      }, [])

    const now = new Date();
    let date = now.getDate().toString();
    let month = (parseInt(now.getMonth().toString()) + 1).toString();
    if(month.length == 1) {
        month = '0' + month;
    }
    if(date.length == 1) {
        date = '0' + date;
    }
    const year = now.getFullYear().toString();

    function deleteStudent() {
        let text = "Are you sure you want to delete?";
        if(confirm(text) == true) {
            const api = 'https://dummyjson.com/users/';
            fetch(api + student.id,{
                method: 'DELETE',
            })
            .then(res => res.json())
            .then(() => {
                console.log();
                history.back();
            });
        }
    }
    
    return(
        <div className={StudentStyle.container}>
            <div className={StudentStyle.title}>
                <a href="/">Student management</a>
            </div>
            <div className={StudentStyle.content}>
                <div className={StudentStyle.avt}>
                    <div>
                        <img src={student.image} alt="" />
                    </div>
                </div>
                <div className={StudentStyle.infor}>
                    <div className={StudentStyle.about}>
                        <p>About</p>
                        <div>
                            <button>
                                <Link href={{pathname: 'edit_student', query: {id: student.id}}}>
                                    Edit
                                </Link> 
                            </button>
                            <button onClick={() => deleteStudent()}>
                                Delete
                            </button>
                        </div>
                    </div>
                    <div className={StudentStyle.name}>
                        <p>{student.firstName} {student.lastName}</p>
                    </div>
                    <table className={StudentStyle.tbl}>
                        <colgroup>
                            <col width={300}/>
                            <col width='*'/>
                        </colgroup>
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>First name: {student.firstName}</td>
                                <td>Last name: {student.lastName}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Email: {student.email}</td>
                            </tr>
                            <tr>
                                <td>Age: {student.age}</td>
                                <td>Gender: {student.gender}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Birthdate: {student.birthDate}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Phone: {student.phone}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Addres: {student.address.address} - {student.address.city}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className={StudentStyle.health}>
                        <p>Health</p>
                    </div>
                    <table className={StudentStyle.tbl}>
                        <colgroup>
                            <col width={300}/>
                            <col width='*'/>
                        </colgroup>
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>Weight: {student.weight} kg</td>
                                <td>Height: {student.height} cm</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Blood Group: {student.bloodGroup}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>Hair: {student.hair.type} {student.hair.color}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> 
            <div className={StudentStyle.footer}>
                <p>{date} - {month} - {year}</p>
            </div>
        </div>
    )
}