import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface LoginStatus {
    status: number,
    user_now: {
        id: string,
        firstname: string,
        lastname: string,
        username: string,
        password: string,
    },
}

const initialState: LoginStatus = {
    status: 0, // 0: Chưa đăng nhập, 1: Đăng nhập với tài khoản admin (atuny0), 2: Đăng nhập với tài khoản sinh viên
    user_now: {
        id: '',
        firstname: '',
        lastname: '',
        username: '',
        password: '',
    }
}

export const login_status = createSlice({
    name: 'login_status',
    initialState,
    reducers: {
        setLoginStatus: (state, action:PayloadAction<number>) => {
            state.status = action.payload;
        },
        setUsername: (state, action:PayloadAction<string>) => {
            state.user_now.username = action.payload;
        },
        setPassword: (state, action:PayloadAction<string>) => {
            state.user_now.password = action.payload;
        },
        setId: (state, action:PayloadAction<string>) => {
            state.user_now.id = action.payload;
        },
        setFirstname: (state, action:PayloadAction<string>) => {
            state.user_now.firstname = action.payload;
        },
        setLastname: (state, action:PayloadAction<string>) => {
            state.user_now.lastname = action.payload;
        },
    }
})

export const { setLoginStatus, setUsername, setPassword, setId, setFirstname, setLastname} = login_status.actions;
export default login_status.reducer;