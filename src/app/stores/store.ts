import { configureStore } from '@reduxjs/toolkit';
import data from './data';
import index_list from './index_list';
import student from './infor_student';
import indicators from './indicators';
import data_search from './search';
import login_status from './login_status';

export const store = configureStore({
  reducer: {
    data: data,
    index_list: index_list,
    indicators: indicators,
    student: student,
    data_search: data_search,
    login_status: login_status,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch