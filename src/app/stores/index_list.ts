import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

// khởi tạo biến initialState
const initialState = [0]

export const index_list = createSlice({
  name: 'index_list',
  initialState,
  reducers: {
    set_index_list: (state, action: PayloadAction<number>) => {
        state[action.payload] = action.payload +1;
    }
  }
})

// Khai báo hàm vừa tạo
export const { set_index_list } = index_list.actions

export default index_list.reducer