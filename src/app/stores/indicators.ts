import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Indicators {
  index_now: number,
  tabNow: number,
}

// khởi tạo biến initialState
const initialState: Indicators = {
  index_now: 1,
  tabNow: 1,
}

export const index_now = createSlice({
  name: 'indicators',
  initialState,
  reducers: {
    set_index_now: (state, action:PayloadAction<number>) => {
      state.index_now = action.payload;
    },
    set_tab_now: (state, action:PayloadAction<number>) => {
      state.tabNow = action.payload;
    }
  }
})

// Khai báo hàm vừa tạo
export const { set_index_now, set_tab_now } = index_now.actions

export default index_now.reducer