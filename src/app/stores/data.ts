import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Data {
  value: [],
}

// khởi tạo biến initialState
const initialState: Data = {
  value: [],
}

export const data = createSlice({
  name: 'data',
  initialState,
  reducers: {
    setData: (state, action:PayloadAction<[]>) => {
      state.value = action.payload;
    }
  }
})

// Khai báo hàm vừa tạo
export const { setData } = data.actions

export default data.reducer