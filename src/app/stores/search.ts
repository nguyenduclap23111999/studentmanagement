import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Search {
  value: [{}],
  index_list: [number],
  index_now: number,
  status: number,
  advanced: boolean,
  search_key: {
    age: {
      min: number,
      max: number
    },
    gender: string,
    weight: {
      min: number,
      max: number,
    },
    height: {
      min: number,
      max: number,
    },
    bloodGroup: string,
  },
  
}

// khởi tạo biến initialState
const initialState: Search = {
  value: [{}],
  index_list: [1],
  index_now: 1,
  status: 1,
  advanced: false,
  search_key: {
    age: {
      min: 0,
      max: 200,
    },
    gender: 'All',
    weight: {
      min: 0,
      max: 999,
    },
    height: {
      min: 0, 
      max: 999,
    },
    bloodGroup: 'A+ A- B+ B- AB+ AB- O+ O-', 
  },
}

export const data_search = createSlice({
  name: 'data_search',
  initialState,
  reducers: {
    clear_data_search: (state) => {
        state.value = [{}]
    },
    setDataSearch: (state, action:PayloadAction<{}>) => {
      state.value.push(action.payload);
    },
    clear_search_index_list: (state) => {
        state.index_list = [1];
    },
    search_index_list: (state, action: PayloadAction<number>) => {
        state.index_list[action.payload] = action.payload +1;
    },
    search_index_now: (state, action:PayloadAction<number>) => {
        state.index_now = action.payload;
    },
    search_status: (state, action:PayloadAction<number>) => {
        state.status = action.payload;
    },
    search_advanced: (state) => {
        state.advanced = !state.advanced;
    },
    search_age_min: (state, action:PayloadAction<number>) => {
        state.search_key.age.min = action.payload;
    },
    search_age_max: (state, action:PayloadAction<number>) => {
      state.search_key.age.max = action.payload;
    },
    search_gender: (state, action:PayloadAction<string>) => {
      state.search_key.gender = action.payload;
    },
    search_weight_min: (state, action:PayloadAction<number>) => {
      state.search_key.weight.min = action.payload;
    },
    search_weight_max: (state, action:PayloadAction<number>) => {
      state.search_key.weight.max = action.payload;
    },
    search_height_min: (state, action:PayloadAction<number>) => {
      state.search_key.height.min = action.payload;
    },
    search_height_max: (state, action:PayloadAction<number>) => {
      state.search_key.height.max = action.payload;
    },
    search_blood_group: (state, action:PayloadAction<string>) => {
      state.search_key.bloodGroup = action.payload;
    },
  }
})

// Khai báo hàm vừa tạo
export const { clear_data_search, setDataSearch, clear_search_index_list, search_index_list, search_index_now, search_status, search_advanced, search_age_min, search_age_max } = data_search.actions
export const {search_gender, search_weight_min, search_weight_max, search_height_min, search_height_max, search_blood_group} = data_search.actions

export default data_search.reducer