import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Student {
  id: number,
  student: {}
}

// khởi tạo biến initialState
const initialState: Student = {
  id: 1,
  student: {}
}

export const student = createSlice({
  name: 'student',
  initialState,
  reducers: {
    set_id_student: (state, action:PayloadAction<number>) => {
      state.id = action.payload;
    },
    set_infor_student: (state, action:PayloadAction<{}>) => {
        state.student = action.payload;
    }
  }
})

// Khai báo hàm vừa tạo
export const { set_id_student, set_infor_student } = student.actions;

export default student.reducer;