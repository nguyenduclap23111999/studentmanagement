import SearchStudentStyle from "@/app/lib/main/SearchStudent.module.css";
import { set_id_student } from "@/app/stores/infor_student";
import { clear_data_search, clear_search_index_list, search_advanced, search_age_max, search_age_min, search_blood_group, search_gender, search_height_max, search_height_min, search_index_list, search_index_now, search_status, search_weight_max, search_weight_min, setDataSearch } from "@/app/stores/search";
import { RootState } from "@/app/stores/store";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";

export default function SearchStudent() {
    const student_list = useSelector((state: RootState) => state.data.value);
    const data = useSelector((state: RootState) => state.data_search.value);
    const index_list = useSelector((state: RootState) => state.data_search.index_list);
    const index_now = useSelector((state: RootState) => state.data_search.index_now);
    const status = useSelector((state: RootState) => state.data_search.status);
    const is_search_advanced = useSelector((state: RootState) => state.data_search.advanced);
    const search_key = useSelector((state: RootState) => state.data_search.search_key);

    const getFnc = useDispatch();
    let name_keyword = '';
    function setNameKeyword(s: string) {
        name_keyword = s.trim().toUpperCase();
    }

    function compareNumbers(a: string, min: number, max: number) {
        if(parseFloat(a) >= min && parseFloat(a) <= max) {
            return true;
        } else {
            return false;
        }
    }
    function compareString(s: string, t: string) {
        if(s.toLowerCase().includes(t.toLowerCase())) {
            return true;
        } else {
            return false;
        }
    }

    function search() {
        getFnc(clear_data_search());
        getFnc(clear_search_index_list());
        getFnc(search_index_now(1));
        let i = 1;
        getFnc(search_status(1));
        if(name_keyword == '') {
            if(search_key.gender == 'All') {
                student_list.map(st => {
                    if(compareNumbers(st.age, search_key.age.min, search_key.age.max) && compareNumbers(st.weight, search_key.weight.min, search_key.weight.max) 
                    && compareNumbers(st.height, search_key.height.min, search_key.height.max) && compareString(search_key.bloodGroup, st.bloodGroup)) {
                        getFnc(setDataSearch(st));
                        if(i % 10 == 0) {
                            getFnc(search_index_list(i/10));
                        }
                        i++;
                    }
                })
            } else {
                student_list.map(st => {
                    if(compareNumbers(st.age, search_key.age.min, search_key.age.max) && compareNumbers(st.weight, search_key.weight.min, search_key.weight.max) 
                    && compareNumbers(st.height, search_key.height.min, search_key.height.max) && compareString(search_key.bloodGroup, st.bloodGroup)
                    && (st.gender == search_key.gender)) {
                        getFnc(setDataSearch(st));
                        if(i % 10 == 0) {
                            getFnc(search_index_list(i/10));
                        }
                        i++;
                    }
                })
            }
        } else {
            if(search_key.gender == 'All') {
                student_list.map(st => {
                    if(compareNumbers(st.age, search_key.age.min, search_key.age.max) && compareNumbers(st.weight, search_key.weight.min, search_key.weight.max) 
                    && compareNumbers(st.height, search_key.height.min, search_key.height.max) && compareString(search_key.bloodGroup, st.bloodGroup)
                    && (compareString(st.firstName, name_keyword) || compareString(st.lastName, name_keyword)))
                    {
                        getFnc(setDataSearch(st));
                        if(i % 10 == 0) {
                            getFnc(search_index_list(i/10));
                        }
                        i++;
                    }
                })
            } else {
                student_list.map(st => {
                    if(compareNumbers(st.age, search_key.age.min, search_key.age.max) && compareNumbers(st.weight, search_key.weight.min, search_key.weight.max) 
                    && compareNumbers(st.height, search_key.height.min, search_key.height.max) && compareString(search_key.bloodGroup, st.bloodGroup)
                    && (st.gender == search_key.gender) 
                    && (compareString(st.firstName, name_keyword) || compareString(st.lastName, name_keyword))) 
                    {
                        getFnc(setDataSearch(st));
                        if(i % 10 == 0) {
                            getFnc(search_index_list(i/10));
                        }
                        i++;
                    }
                })
            }
        }
    }

    function selectInforStudent(id: number) {
        getFnc(set_id_student(id));
        localStorage.setItem('id_student', id.toString());
    }

    function setIndexNow(n: number) {
        getFnc(search_index_now(n));
    }

    function getAgeMin(s: string) {
        getFnc(search_age_min(parseInt(s)));
        
    }
    function getAgeMax(s: string) {
        getFnc(search_age_max(parseInt(s)));
    }
    function getGender(s: string) {
        getFnc(search_gender(s));
    }
    function getWeightMin(s: string) {
        getFnc(search_weight_min(parseFloat(s)));
    }
    function getWeightMax(s: string) {
        getFnc(search_weight_max(parseFloat(s)));
    }
    function getHeightMin(s: string) {
        getFnc(search_height_min(parseFloat(s)));
    }
    function getHeightMax(s: string) {
        getFnc(search_height_max(parseFloat(s)));
    }
    function getBloodGroup(s: string) {
        getFnc(search_blood_group(s));
    }
    function showMenu() {
        getFnc(search_advanced());
    }

    // tạo 1 biến infor_student trong data search rồi set giá trị mỗi lần onChange

    return(
        <div className="mt-[20px]">
            <div className={SearchStudentStyle.search}>
                <input type="text" onChange={(e) => setNameKeyword(e.target.value)}
                        placeholder="Search by Firstname or Lastname..."
                />
                <button type="submit" onClick={() => search()}>Search</button>
                <button onClick={() => showMenu()}><i className="fa-solid fa-bars"></i></button>
                <div className={is_search_advanced ? SearchStudentStyle.advanced_search : SearchStudentStyle.hide_result}>
                    <h4>Advanced search</h4>
                    <div className={SearchStudentStyle.item_search}>
                        <div>
                            <p>Age: </p>
                            <input type="number" onChange={(e) => getAgeMin(e.target.value)}/>
                            <p>to: </p>
                            <input type="number" onChange={(e) => getAgeMax(e.target.value)}/>
                        </div>
                        <div className={SearchStudentStyle.gender}>
                            <p>Gender: </p>
                            <div>
                                <input type="radio" name="Gender" value="male" onChange={(e) => getGender(e.target.value)}/>
                                <p>Male</p>
                            </div>
                            <div>
                                <input type="radio" name="Gender" value="female" onChange={(e) => getGender(e.target.value)}/>
                                <p>Female</p>
                            </div>
                            <div>
                                <input type="radio" name="Gender" value="All" onChange={(e) => getGender(e.target.value)}/>
                                <p>All</p>
                            </div>
                        </div>
                        <div className={SearchStudentStyle.weight}>
                            <p>Weight (kg): </p>
                            <input type="number" min={1} max={999} onChange={(e) => getWeightMin(e.target.value)}/>
                            <p>to:</p>
                            <input type="number" min={1} max={999} onChange={(e) => getWeightMax(e.target.value)}/>
                        </div>
                        <div className={SearchStudentStyle.height}>
                            <p>Height (cm): </p>
                            <input type="number" min={1} max={999} onChange={(e) => getHeightMin(e.target.value)}/>
                            <p>to:</p>
                            <input type="number" min={1} max={999} onChange={(e) => getHeightMax(e.target.value)}/>
                        </div>
                        <div>
                            <p>Blood Group: </p>
                            <select name="bloodGroup" id="" onChange={(e) => getBloodGroup(e.target.value)}>
                                <option value="All">Chọn</option>
                                <option value="A+">A+</option>
                                <option value="A-">A-</option>
                                <option value="B+">B+</option>
                                <option value="B-">B-</option>
                                <option value="AB+">AB+</option>
                                <option value="AB-">AB-</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                            </select>
                        </div>
                    </div>
                    <div className={SearchStudentStyle.btn_apply}>
                        <button onClick={() => showMenu()}>Apply</button>
                    </div>
                </div>
            </div>
            <div className={status == 1 ? SearchStudentStyle.search_result : SearchStudentStyle.hide_result}>
                <div className={SearchStudentStyle.tbl}>
                    <table cellPadding={0} cellSpacing={0}>
                        <colgroup>
                            <col width={100}/>
                            <col width={150}/>
                            <col width={150}/>
                            <col width={150}/>
                            <col width={150}/>
                            <col width="*"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th className={SearchStudentStyle.td1}>ID</th>
                                <th className={SearchStudentStyle.td2}>First Name</th>
                                <th className={SearchStudentStyle.td3}>Last Name</th>
                                <th className={SearchStudentStyle.td4}>Age</th>
                                <th className={SearchStudentStyle.td5}>Gender</th>
                                <th className={SearchStudentStyle.td6}>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((st) => {
                                if(data.indexOf(st) >= (index_now*10-9) && data.indexOf(st) <= (index_now*10)) {
                                    return(
                                        <tr key={st.id} onClick={() => selectInforStudent(st.id)}>
                                            <td className={SearchStudentStyle.td1}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.id}
                                                </Link>    
                                            </td>
                                            <td className={SearchStudentStyle.td2}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.firstName}
                                                </Link>    
                                            </td>
                                            <td className={SearchStudentStyle.td3}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.lastName}
                                                </Link>
                                            </td>
                                            <td className={SearchStudentStyle.td4}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.age}
                                                </Link>
                                            </td>
                                            <td className={SearchStudentStyle.td5}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.gender}
                                                </Link>    
                                            </td>
                                            <td className={SearchStudentStyle.td6}>
                                                <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                    {st.email}
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                }
                            })}
                        </tbody>
                    </table>
                </div>
                <div className={SearchStudentStyle.index_list}>
                    {index_list.map(i => {
                        return(
                            <div key={i} onClick={() => setIndexNow(i)}
                                className={index_now == i ? SearchStudentStyle.index_now : SearchStudentStyle.index_default}
                            >
                                <p>{i}</p>
                            </div>
                        )
                    })}
                </div>
            </div>
            
        </div>
    )
}