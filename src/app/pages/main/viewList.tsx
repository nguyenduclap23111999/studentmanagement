import { RootState } from "@/app/stores/store"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ViewListStyle from "@/app/lib/main/ViewList.module.css";
import { setData } from "@/app/stores/data";
import { set_index_now } from "@/app/stores/indicators";
import { set_id_student } from "@/app/stores/infor_student";
import Link from "next/link";
import router from "next/router";

export default function ViewList() {
    const student_list = useSelector((state: RootState) => state.data.value);
    const index_list = useSelector((state: RootState) => state.index_list);
    const index_now = useSelector((state: RootState) => state.indicators.index_now);
    const getFnc = useDispatch();

    function setIndexNow(n: number) {
        getFnc(set_index_now(n));
    }

    function selectInforStudent(id: number) {
        getFnc(set_id_student(id));
        localStorage.setItem('id_student', id.toString());
    } 

    return(
        <div className="">
            <div className={ViewListStyle.tbl}>
                <table cellPadding={0} cellSpacing={0}>
                    <colgroup>
                        <col width={100}/>
                        <col width={150}/>
                        <col width={150}/>
                        <col width={150}/>
                        <col width={150}/>
                        <col width="*"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th className={ViewListStyle.td1}>ID</th>
                            <th className={ViewListStyle.td2}>First Name</th>
                            <th className={ViewListStyle.td3}>Last Name</th>
                            <th className={ViewListStyle.td4}>Age</th>
                            <th className={ViewListStyle.td5}>Gender</th>
                            <th className={ViewListStyle.td6}>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {student_list.map((st) => {
                            if(st.id >= (index_now*10-9) && st.id <= (index_now*10)) {
                                return(
                                    <tr key={st.id} onClick={() => selectInforStudent(st.id)}>
                                        <td className={ViewListStyle.td1}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.id}
                                            </Link>    
                                        </td>
                                        <td className={ViewListStyle.td2}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.firstName}
                                            </Link>    
                                        </td>
                                        <td className={ViewListStyle.td3}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.lastName}
                                            </Link>
                                        </td>
                                        <td className={ViewListStyle.td4}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.age}
                                            </Link>
                                        </td>
                                        <td className={ViewListStyle.td5}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.gender}
                                            </Link>    
                                        </td>
                                        <td className={ViewListStyle.td6}>
                                            <Link href={{pathname: 'student', query: {id: st.id}}}>
                                                {st.email}
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            }
                        })}
                    </tbody>
                </table>
            </div>
            <div className={ViewListStyle.index_list}>
                {index_list.map(i => {
                    return(
                        <div key={i} onClick={() => setIndexNow(i)}
                            className={index_now == i ? ViewListStyle.index_now : ViewListStyle.index_default}
                        >
                            <p>{i}</p>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}