import AddNewStyle from "@/app/lib/main/AddNewStudent.module.css";
import { useState } from "react";
export default function AddNewStudent() {   
    const [avt, setAvt] = useState('https://images.rawpixel.com/image_800/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTA1L3Y5MzctYWV3LTEzOS5qcGc.jpg');
    const avt1 = 'https://i.pinimg.com/564x/2b/0f/7a/2b0f7a9533237b7e9b49f62ba73b95dc.jpg';
    const avt2 = 'https://chiemtaimobile.vn/images/companies/1/%E1%BA%A2nh%20Blog/avatar-facebook-dep/Anh-avatar-hoat-hinh-de-thuong-xinh-xan.jpg?1704788263223';
    const avt3 = 'https://cdn.sforum.vn/sforum/wp-content/uploads/2023/11/avatar-dep-107.jpg';
    const avt4 = 'https://hoanghamobile.com/tin-tuc/wp-content/uploads/2023/07/avatar-dep-4.jpg';
    const avt5 = 'https://gcs.tripi.vn/public-tripi/tripi-feed/img/474103Wsh/hinh-avatar-dep-1.jpg';
    const avt6 = 'https://kiemtientuweb.com/ckfinder/userfiles/images/avatar-fb/avatar-fb-1.jpg';
    const [isListAvt, setIsListAvt] = useState(false);
    let student = {
        firstName: '',
        lastName: '',
        age: '',
        birthDate: '',
        gender: 'male',
        email: '',
        image: avt,
        phone: '',
        address: {
            address: '',
            city: ''
        },
        weight: '',
        height: '',
        bloodGroup: '',
        hair: {
            color: '',
            type: '' 
        }
    }
    const [age, setAge] = useState('');
    const now = new Date();
    const day_now = now.getFullYear().toString().trim() + "-12-31";
    function showAvt(s: string) {
        setAvt(s);
        setIsListAvt(false);
        student.image = s;
    }
    function showSelectAvt() {
        setIsListAvt(!isListAvt);
    }
    function setFirstName(s: string) {
        student.firstName = s.trim();
    }
    function setLastName(s: string) {
        student.lastName = s.trim();
    }
    function setEmail(s: string) {
        student.email = s.trim();
    }
    function setGender(s: string) {
        student.gender = s;
    }
    function setBirthDate(s: string) {
        student.birthDate = s;
        student.age = (now.getFullYear() - parseInt(s.slice(0, 4))).toString();
        setAge(student.age);
        console.log(day_now);
        
    }
    function setPhone(s: string) {
        student.phone = s.trim();
    }
    function setAddressAdress(s: string) {
        student.address.address = s.trim();
    }
    function setAddressCity(s: string) {
        student.address.city = s.trim();
    }
    function setWeight(s: string) {
        student.weight = s;
    }
    function setHeight(s: string) {
        student.height = s;
    }
    function setBloodGroup(s: string) {
        student.bloodGroup = s;
    }
    function setHairColor(s: string) {
        student.hair.color = s.trim();
    }
    function setHairType(s: string) {
        student.hair.type = s.trim();
    }

    function addNewStudent() {
        if(student.firstName == "" || student.lastName == "" || student.age == "" || student.email == "" || student.phone == "" || student.address.address == "" || student.address.city == "" || student.weight == "" || student.height == "" || student.hair.color == "" || student.hair.type == "" ) {
            alert("You must enter all information completely");
        } else { 
            try{
                fetch('https://dummyjson.com/users/add', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                    firstName: student.firstName,
                    lastName: student.lastName,
                    age: student.age,
                    birthDate: student.birthDate,
                    gender: student.gender,
                    email: student.email,
                    image: student.image,
                    phone: student.phone,
                    address: {
                        address: student.address.address,
                        city: student.address.city
                    },
                    weight: student.weight,
                    height: student.height,
                    bloodGroup: student.bloodGroup,
                    hair: {
                        color: student.hair.color,
                        type: student.hair.type
                    }
                    })
                })
                .then(res => res.json())
                .then(console.log)
                alert("Added new students successfully!!");
            } catch(e) {
                alert("Adding new students failed!!");
                console.log(e);
            } finally {
                student = {
                    firstName: '',
                    lastName: '',
                    age: '',
                    birthDate: '',
                    gender: 'male',
                    email: '',
                    image: avt,
                    phone: '',
                    address: {
                        address: '',
                        city: ''
                    },
                    weight: '',
                    height: '',
                    bloodGroup: '',
                    hair: {
                        color: '',
                        type: '' 
                    }
                }
            }
        }
    }

    return(
        <div className={AddNewStyle.content}>
            <div>
                <div className={AddNewStyle.avt}>
                    <img src={avt} alt="" />
                </div>
                <div className={AddNewStyle.select_avt}>
                    <button onClick={() => showSelectAvt()}>Select an image</button>
                    <div className={isListAvt ? AddNewStyle.list_avt : AddNewStyle.list_avt_hide}>
                        <div className={AddNewStyle.img1} onClick={() => showAvt(avt1)}>Img 1</div>
                        <div className={AddNewStyle.img2} onClick={() => showAvt(avt2)}>Img 2</div>
                        <div className={AddNewStyle.img3} onClick={() => showAvt(avt3)}>Img 3</div>
                        <div className={AddNewStyle.img4} onClick={() => showAvt(avt4)}>Img 4</div>
                        <div className={AddNewStyle.img5} onClick={() => showAvt(avt5)}>Img 5</div>
                        <div className={AddNewStyle.img6} onClick={() => showAvt(avt6)}>Img 6</div>
                    </div>
                </div>
            </div>
            <div className={AddNewStyle.infor}>
                <div className={AddNewStyle.about}>
                    <p>About</p>
                </div>
                <table className={AddNewStyle.tbl}>
                    <colgroup>
                        <col width={100}/>
                        <col width={280}/>
                        <col width={100}/>
                        <col width='*'/>
                    </colgroup>
                    <thead></thead>
                    <tbody>
                        <tr className={AddNewStyle.tr1}>
                            <td>First name:</td>
                            <td>
                                <input type="text" id="inp_firstName" onChange={(e) => setFirstName(e.target.value)}/>
                            </td>
                            <td>Last name:</td>
                            <td>
                                <input type="text" id="inp_lastName" onChange={(e) => setLastName(e.target.value)}/>
                            </td>
                        </tr>
                        <tr className={AddNewStyle.tr2}>
                            <td>Email:</td>
                            <td colSpan={3}>
                                <input type="email" onChange={(e) => setEmail(e.target.value)}/>
                            </td>
                        </tr>
                        <tr className={AddNewStyle.tr4}>
                            <td>Birthdate:</td>
                            <td colSpan={3}>
                                <input type="date" 
                                       onChange={(e) => setBirthDate(e.target.value)}
                                       min="1990-01-01"
                                       max={day_now}
                                />
                            </td>
                        </tr>
                        <tr className={AddNewStyle.tr3}>
                            <td>Age:</td>
                            <td>
                                <input type="text" id="inp_age" placeholder={age} disabled/>
                            </td>
                            <td>Gender:</td>
                            <td>
                                <input type="radio" name="gender" id="inp_male" value="male" defaultChecked onChange={(e) => setGender(e.target.value)} />
                                <label htmlFor="">Male</label>
                                <input type="radio" name="gender" id="inp_female" value="female" onChange={(e) => setGender(e.target.value)}/>
                                <label htmlFor="">Female</label>
                            </td>
                        </tr>
                        <tr className={AddNewStyle.tr5}>
                            <td>Phone:</td>
                            <td colSpan={3}>
                                <input type="text" id="inp_phone" onChange={(e) => setPhone(e.target.value)}/>
                            </td>
                        </tr>
                        <tr className={AddNewStyle.tr6}>
                            <td>Address:</td>
                            <td>
                                <input type="text" id="inp_address" onChange={(e) => setAddressAdress(e.target.value)}/>
                            </td>
                            <td>City:</td>
                            <td>
                                <input type="text" onChange={(e) => setAddressCity(e.target.value)}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className={AddNewStyle.health}>
                    <p>Health</p>
                </div>
                <table className={AddNewStyle.tbl}>
                    <colgroup>
                        <col width={100}/>
                        <col width={280}/>
                        <col width={100}/>
                        <col width='*'/>
                    </colgroup>
                    <thead></thead>
                    <tbody>
                        <tr>
                            <td>Weight (kg): </td>
                            <td>
                                <input type="number" id="inp_weight" max={1000} min={1} onChange={(e) => setWeight(e.target.value)}/>
                            </td>
                            <td>Height (cm): </td>
                            <td>
                                <input type="number" id="inp_height" min={1} onChange={(e) => setHeight(e.target.value)}/>
                            </td>
                        </tr>
                        <tr>
                            <td>Blood Group: </td>
                            <td colSpan={3}>
                                <select name="blood_group" id="slc_bloodGroup" onChange={(e) => setBloodGroup(e.target.value)}>
                                    <option value="select">Select</option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Hair color:</td>
                            <td>
                                <input type="text" id="inp_hairColor" onChange={(e) => setHairColor(e.target.value)}/>
                            </td>
                            <td>Hair type:</td>
                            <td>
                                <input type="text" id="inp_hairType" onChange={(e) => setHairType(e.target.value)}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className={AddNewStyle.btn}>
                    <button onClick={() => addNewStudent()}>Add new student</button>
                    <button>Cancel</button>
                </div>
            </div>
        </div>
    )
}