import LoginStyle from "@/app/lib/Login.module.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../stores/store";
import { setFirstname, setId, setLastname, setLoginStatus, setPassword, setUsername } from "../stores/login_status";

export default function Login() {
    const login = useSelector((state: RootState) => state.login_status);
    const getFnc = useDispatch();

    function getUsername(s: string) {
        getFnc(setUsername(s.trim()));
    }
    function getPassword(s: string) {
        getFnc(setPassword(s.trim()));
    }
    function getUsernow(status: number ,id: string, username: string, firstname: string, lastname: string) {
        getFnc(setLoginStatus(status));
        getFnc(setId(id));
        getFnc(setUsername(username));
        getFnc(setFirstname(firstname));
        getFnc(setLastname(lastname));
    }

    function setUsernowLocal(id: string, firstname: string, lastname: string) {
        localStorage.setItem('id_login', id);
        localStorage.setItem('name_login', firstname + lastname);
    }

    function clickLogin() {
        if(login.user_now.username == '' || login.user_now.password == '') {
            alert("Wrong username or password !!")
        } else {
            const api = 'https://dummyjson.com/users/filter?key=username&value=';
            const select = '&select=id,username,password,firstName,lastName'
            try {
                fetch(api + login.user_now.username + select)
                .then(res => res.json())
                .then(data => {
                    if(data.users.length > 0) {
                        if(data.users[0].password == login.user_now.password) {
                            getUsernow(1, data.users[0].id, data.users[0].username, data.users[0].firstName, data.users[0].lastName);
                            setUsernowLocal(data.users[0].id, data.users[0].firstName, data.users[0].lastName);
                            alert("Logged in successfull !!");
                        } else {
                            alert("Wrong account or password !!");
                        }
                    } else {
                        setUsernowLocal('0','0','0');
                        alert("Wrong account or password !!");
                    }
                })
            } catch {
                alert('An error has occurred !!');
            }
        }
        
    }
    return(
        <div className={LoginStyle.content}>
            <div className={LoginStyle.bg}>
                bg
            </div>
            <div className={LoginStyle.div_login}>
                <div className={LoginStyle.title}>
                    <h3>Student Managenent</h3>
                </div>
                <div className={LoginStyle.form_login}>
                    <div>
                        <p>Username: </p>
                        <input type="text" onChange={(e) => getUsername(e.target.value)}/>
                    </div>
                    <div>
                        <p>Password: </p>
                        <input type="password" onChange={(e) => getPassword(e.target.value)}/>
                    </div>
                    <div className={LoginStyle.forget_pass}>
                        <a href="#">Quên mật khẩu ?</a>
                    </div>
                    <div className={LoginStyle.btn}>
                        <button onClick={() => clickLogin()}>Login</button>
                    </div>
                </div>
                <div className={LoginStyle.credit}>
                    <p>Product created by Lapnd</p>
                </div>
            </div>
        </div>
    )
}