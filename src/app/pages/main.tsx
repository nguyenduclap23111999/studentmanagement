'use client'

import MainStyle from '@/app/lib/Main.module.css'
import ViewList from './main/viewList';
import { useSelector } from 'react-redux';
import { RootState } from '../stores/store';
import AddNewStudent from './main/addNewStudent';
import SearchStudent from './main/searchStudent';

export default function Main() {
    const tab_now = useSelector((state: RootState) => state.indicators.tabNow);
    let tab = <ViewList />
    if(tab_now == 1) {
        tab = <ViewList />
    } else if(tab_now == 2) {
        tab = <AddNewStudent />
    } else {
        tab = <SearchStudent />
    }

    return(
        <div className={MainStyle.content}>
            {tab}      
        </div>
    )
}