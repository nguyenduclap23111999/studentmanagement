'use client'

import { useSelector } from "react-redux";
import { RootState } from "../stores/store";
import FooterStyle from "@/app/lib/Footer.module.css";

export default function Footer() {
    const status = useSelector((state: RootState) => state.data_search.status);

    const now = new Date();
    let date = now.getDate().toString();
    let month = (parseInt(now.getMonth().toString()) + 1).toString();
    if(month.length == 1) {
        month = '0' + month;
    }
    if(date.length == 1) {
        date = '0' + date;
    }
    const year = now.getFullYear().toString();
    
    return(
        <div className={status == 1 ? FooterStyle.default : FooterStyle.fixed}>
            <div className="mt-[20px] p-[10px] text-center bg-[#0ccc8c] text-[13px] text-[white]">
                <p>{date} - {month} - {year}</p>
            </div>
        </div>
    )
}