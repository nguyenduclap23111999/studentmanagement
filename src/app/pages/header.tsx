'use client'

import HeaderStyle from '@/app/lib/Header.module.css'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import data, { setData } from '../stores/data';
import { set_index_list } from '../stores/index_list';
import { RootState } from '../stores/store';
import { set_tab_now } from '../stores/indicators';
import { search_status } from '../stores/search';
import { setFirstname, setId, setLastname, setLoginStatus, setUsername } from '../stores/login_status';
import Link from 'next/link';

export default function Header() {
    const student_list = useSelector((state: RootState) => state.data.value);
    const tab_now = useSelector((state: RootState) => state.indicators.tabNow);
    const login = useSelector((state: RootState) => state.login_status);
    const getFnc = useDispatch();
    const api = 'https://dummyjson.com/users?limit=100&select=firstName,lastName,age,gender,email,weight,height,bloodGroup'
    useEffect(() => {
        fetch(api)
        .then((res) => res.json())
        .then((data) => {
            getFnc(setData(data.users));
            for(let i = 0; i < data.users.length/10; i ++) {
                getFnc(set_index_list(i));
            }
        });
    }, [])

    const name = localStorage.getItem('name_login');
    const id = localStorage.getItem('id_login');

    function setTab(t: number, stt: number) {
        getFnc(set_tab_now(t));
        getFnc(search_status(stt));
    }

    function getUsernow(status: number ,id: string, username: string, firstname: string, lastname: string) {
        getFnc(setLoginStatus(status));
        getFnc(setId(id));
        getFnc(setUsername(username));
        getFnc(setFirstname(firstname));
        getFnc(setLastname(lastname));
    }

    function setUsernowLocal(id: string, firstname: string, lastname: string) {
        localStorage.setItem('id_login', id);
        localStorage.setItem('name_login', firstname + lastname);
    }
    
    function logout() {
        getUsernow(0, '', '', '', '');
        setUsernowLocal('', '', '');
    }

    function goToStudent(s: string) {
        localStorage.setItem('id_student', s);
    }    

    return(
        <div className={HeaderStyle.content}>
            <div className={HeaderStyle.logo}>
                <a href='/'>Student Management</a>
                <div>
                    {/* <a href="#">{login.user_now.firstname} {login.user_now.lastname}</a> */}
                    <span>{name}</span>
                    <p>▼</p>
                    <div className={HeaderStyle.private_menu}>
                        <p onClick={() => goToStudent(id)}>
                            <Link href={{pathname: 'student', query: {id: id}}}>
                                Personal page
                            </Link>
                        </p>
                        <p>
                            <Link href={{query: {id: id}, pathname: 'message'}}>
                                Message
                            </Link>
                        </p>
                        <p><a href="" onClick={() => logout()}>Log out</a></p>
                    </div>
                </div>
            </div>
            <div className='w-[100%]'>
                <ul className={HeaderStyle.menu}>
                    <li className={tab_now == 1 ? HeaderStyle.tab_now : HeaderStyle.tab_default}
                        onClick={() => setTab(1, 1)}
                    >
                        View Student List
                    </li>
                    <li className={tab_now == 2 ? HeaderStyle.tab_now : HeaderStyle.tab_default}
                        onClick={() => setTab(2, 1)}
                    >
                        Add new Student
                    </li>
                    <li className={tab_now == 3 ? HeaderStyle.tab_now : HeaderStyle.tab_default}
                        onClick={() => setTab(3, 0)}
                    >
                        Search for Student
                    </li>
                </ul>
            </div>
        </div>
    )
}