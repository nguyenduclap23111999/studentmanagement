'use client'

import { useSelector } from "react-redux";
import Footer from "./pages/footer";
import Header from "./pages/header";
import Login from "./pages/login";
import Main from "./pages/main";
import PageStyle from "@/app/lib/Page.module.css";
import { RootState } from "./stores/store";

export default function Home() {
  const login_status = useSelector((state: RootState) => state.login_status);
  const login = localStorage.getItem('id_login');
  console.log(localStorage.getItem('id_login'));
  
  return (
    <div>
      <div className={(login_status.status == 1 || login != '') ? PageStyle.show : PageStyle.hide}>
        <Header />
        <Main />
        <Footer />
      </div>
      <div className={login == '' ? PageStyle.show : PageStyle.hide}>
        <Login />
      </div>
    </div>
  );
}
